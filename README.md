# Project name

> OGL ships 3D


## Spis treści

* [Ogólne informacje](#ogólne-informacje)
* [Użyte technologie](#użyte-technologie)
* [Screenshots](#screenshots)
* [Kontakt](#Kontakt)

<a name="ogólne-informacje"></a>
##  Ogólne informacje
-- Statki 3D. Gra  została zaprojektowana na podstawie wymagań oraz własnych założeń. Celem gry jest jak najszybsze zatopienie okrętów przeciwnika. Gra przebiega naprzemiennie. Najpierw my "oddajemy strzał" na  plansze przeciwnika(w tym  przypadku komputer), następnie wykonywany  jest ruch przeciwnika.



<a name="screenshots"></a>
## Screenshots

![Ustawianie statku](images/ships1.PNG)

> Prawym przyciskiem myszy ustawiamy nasz statek na planszy.

![Rotacja statku](images/ships2.PNG)
* 
> Używając klawiszu "R" na klawiaturze, wykonujemy  rotacje statku.

![Ustawienie kamery i strzał](images/ships3.PNG)
* 
> Używając  klawiszy  WSAD poruszamy się kamerą w  wybranym kierunku. Shift porusza kamerą w dół a spacja w  górę.

![ships4](images/ships4.PNG)
*
> Gra vs komputer

<a name="użyte-technologie"></a>
##  Użyte technologie
- OpenGL

<a name="Wymagania></a>


<a name="status-projektu"></a>

<a name="Kontakt"></a>
## Kontakt
mateuszczosn@gmail.com


Mateusz       							| Daniel
------------- 							| -------------
Ładowanie tekstur - klasa texture				|Wyświetlanie obiektów vao,vbo, ebo,  mesh
Ładowanie shaderów i komunikacja z shaderami - klasa shader	|Klasa przechowująca pola graczy - klasa array
Ładowanie obiektów 3D - klasa  model				|Strzelanie przez komputer - funkcja shootP
Poruszanie kamery - klasa camera				|Stawianie statku przez komputer - funkcja  setShips
Resetowanie gry - funkcja  restartWindow			|Dokumentacja
Główna implementacja kodu - funkcja main			







